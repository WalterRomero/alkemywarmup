import React,{useState} from 'react'
import {Modal,Button,InputGroup,FormControl} from 'react-bootstrap'
import { connect } from 'react-redux';
import postActions from '../redux/actions/postActions'
import Swal from 'sweetalert2'
const ModalAdd = (props) => {
  
    const [show, setShow] = useState(false);
    const [addPost, setAddPost] = useState({})

    const inputNew = e =>{
        e.preventDefault()
        const campo = e.target.name 
        const valor = e.target.value
        setAddPost({
          ...addPost,
          [campo]: valor

        })
    }

    const sendPost = e =>{
      e.preventDefault();
      if(!addPost.title || !addPost.content){
        Swal.fire({
          icon: 'error',
          title: 'Please complete all fields.',
          showConfirmButton: false,
          timer: 4000
        }) } else{
          props.newPost(addPost)
          setShow(false)
        }

    }
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                Add Post
            </Button>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Hi! Ready to add a new post? </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <InputGroup size="df" className="mb-3">
        <InputGroup size="lg">
    <InputGroup.Prepend>
      <InputGroup.Text id="inputGroup-sizing-lg">Title</InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl aria-label="Large" aria-describedby="inputGroup-sizing-sm" onChange={inputNew} name='title'/>
  </InputGroup>
  <br/>
    <InputGroup.Prepend>
      <InputGroup.Text>Content</InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl as="textarea" aria-label="With textarea" onChange={inputNew} name='content'/>
  </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={sendPost}>
            Add!
          </Button>
        </Modal.Footer>
      </Modal>
        </>
    )
}

const mapStateToProps =state =>{
  return{
      blogPost: state.postB.blogPost
      
  }
 }
 const mapDispatchToProps ={
  listPost: postActions.listPost,
  removePost:postActions.removePost,
  newPost:postActions.newPost
 }
export default connect(mapStateToProps,mapDispatchToProps) (ModalAdd)
