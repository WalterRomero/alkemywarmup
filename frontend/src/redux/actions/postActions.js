import axios from 'axios'

const postActions = {
    listPost: () =>{
        return async (dispatch, getState) =>{
            const data = await axios.get('http://localhost:4000/api/posts/')
            dispatch({type: 'ALL_POST', payload: data.data})
        }
    },
    newPost: post =>{
        return async (dispatch, getState) =>{
            const data = await axios.post('http://localhost:4000/api/posts/',post)
            dispatch({type: 'NEW_POST', payload: data.data})
        }
    },
    removePost: id =>{
        return async(dispatch,getState)=>{
            const data = await axios.delete('http://localhost:4000/api/posts/'+id)
            dispatch({type:'DELETE_POST',payload:data.data})
        }
    },
    editPost:(id,newEdited)=>{

        return async (dispatch, getState) =>{
          const data = await axios.put('http://localhost:4000/api/posts/'+id,newEdited)
            dispatch({type: 'EDIT_POST', payload: data.data})
        } 
},
}
export default postActions