import React from 'react'
import './App.css';
import { Header } from './Components/Header';
import Home from './Components/Home';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import  Details  from './Components/Details';
import Edit from './Components/Edit';
const App = () => {
  return (
    <>
      <Header/>
      <BrowserRouter>
        <Switch>
          <Route exact path='/details/:id' component={Details}/>
          <Route exact path='/' component={Home}/>
          <Route exact path='/edit/:id' component={Edit}/>
          <Redirect to='/'/>
        </Switch>
      </BrowserRouter>
    </>
  )
}

export default App
