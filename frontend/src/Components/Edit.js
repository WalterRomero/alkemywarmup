import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import postActions from '../redux/actions/postActions'
import Swal from 'sweetalert2'
import {InputGroup,FormControl, Button} from 'react-bootstrap'
const Edit = (props) => {
    const [gEdit, setEdit] = useState({})
    const [newEdited,setNewEdited] = useState({})
    const {id} = props.match.params
    const blogEdit = props.blogPost.filter(posts => posts.id == id)
    useEffect(()=>{
        if(blogEdit.length>0){
            setEdit(blogEdit[0]) 
        }else{
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Looks like you got lost!',
                showConfirmButton: false,
                timer: 1500
              })
              props.history.push('/')
        }
    },[blogEdit])
    const capEntry = e =>{
        e.preventDefault()
        const campo = e.target.name
        const valor = e.target.value
        setNewEdited({
            ...newEdited,
            [campo] : valor,
            
        })
    }
    const edited = (id) =>{
        if(!newEdited.title || !newEdited.content){
            Swal.fire({
              icon: 'error',
              title: 'Please complete all fields.',
              showConfirmButton: false,
              timer: 4000
            }) } else{
                props.editPost(id,newEdited)
                props.history.push('/')
            }
    }
    return (
        
        <div className='containerEdit'>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default" onChange={capEntry} name='title'>{gEdit.title}</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl onChange={capEntry} name='title'
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default"onChange={capEntry}name='content'>{gEdit.content}</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl onChange={capEntry}name='content'
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
                />
            </InputGroup>
            <Button onClick={()=>edited(gEdit.id)}>Edit</Button>
        </div>
    )
}

const mapStateToProps =state =>{
    return{
        blogPost: state.postB.blogPost
        
    }
   }
   const mapDispatchToProps ={
    listPost: postActions.listPost,
    removePost:postActions.removePost,
    newPost:postActions.newPost,
    editPost:postActions.editPost
   }
export default connect(mapStateToProps,mapDispatchToProps) (Edit)
