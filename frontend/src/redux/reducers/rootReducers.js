import {combineReducers} from 'redux'
import postReducers from './postReducers'


const rootReducer = combineReducers({
    postB: postReducers
})

export default rootReducer