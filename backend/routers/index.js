const express= require('express')
const router = express.Router()
const postController = require('../controllers/postController')


router.route('/posts')
.get(postController.getPost)
.post(postController.setPost)
router.route('/posts/:id')
.get(postController.getOne)
.put(postController.editPost)
.delete(postController.deletePost)

module.exports = router