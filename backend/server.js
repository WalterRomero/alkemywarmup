const express = require('express');
const router = require('./routers');
const app = express();
const cors = require ('cors')
require('./configs/db')

app.use(cors())
app.use(express.json());

app.use('/api',router)
app.listen(4000, ()=> console.log("App listening on port 4000"))

