const {Post} = require('../configs/db');
const post = require('../models/post');

const postController = {
    getPost : async (req,res) =>{
        const post = await Post.findAll();
        res.json(post)
    },
    setPost: async (req,res) =>{
        const post = await Post.create(req.body)
        res.json(post)
    },
    editPost: async (req,res) =>{
        await Post.update(req.body,{
            where:{id:req.params.id}
        })
        res.json({success:true})
    },
    deletePost: async(req,res)=>{
        await Post.destroy({
            where: {id:req.params.id}
        })
        res.json({success:true})
    },
    getOne: async(req,res) =>{
        const post= await Post.findOne({
            where:{id:req.params.id}
        })
        res.json(post)
    }
}

module.exports  = postController