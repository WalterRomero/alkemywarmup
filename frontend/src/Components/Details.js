import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import postActions from '../redux/actions/postActions'
import Swal from 'sweetalert2'
const Details = (props) => {
    const [gDetails, setDetails] = useState({})
    const {id} = props.match.params 
    const blogDetails = props.blogPost.filter(blogPost => blogPost.id == id) 
    useEffect(()=>{
        if(blogDetails.length>0){
            setDetails(blogDetails[0])
        } else{
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Looks like you got lost!',
                showConfirmButton: false,
                timer: 1500
              })
              props.history.push('/')
        }
    
    },[blogDetails])
    return (
        
        <div className='containerDetails'>
            <div className='informationDetails'>
                <h3 >{gDetails.title}</h3>
                <div>
                <p>{gDetails.content}</p>
                </div>
            </div>
        </div>
    )
}


const mapStateToProps =state =>{
    return{
        blogPost: state.postB.blogPost
    }
   }
   const mapDispatchToProps ={
    listPost: postActions.listPost
   }
export default connect(mapStateToProps,mapDispatchToProps) (Details)
