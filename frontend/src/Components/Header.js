import React from 'react'
import {Navbar,Container} from 'react-bootstrap'
import ModalAdd from './Modal'
export const Header = () => {
    return (
        <header>
            <Navbar expand="lg" bg="light">
                <Container>
                    <Navbar.Brand href="/">BlogTap</Navbar.Brand>
                    <ModalAdd/>
                </Container>
            </Navbar>
        </header>
    )
}
